#ifndef CLUSTER_H
#define CLUSTER_H

struct Cluster {
	double df;  /* fractional dimension */
	double *rG; /* Center of gravity */
	double RG2; /* Radius of gyration */
	int length;
	double** points;
	int alloc_length;
};

struct Cluster allocate_cluster(int length);
struct Cluster copy_cluster(struct Cluster clust);
void free_cluster(struct Cluster clust);

void print_cluster(struct Cluster clust);
void print_cluster_to_file(const char *file, struct Cluster clust);
struct Cluster read_cluster_from_file(const char *file);

void add_point_cluster(struct Cluster *clust, double *point);
void translate_cluster(struct Cluster clust, double *point);

#endif

