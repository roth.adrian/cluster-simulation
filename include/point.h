#ifndef POINT_H
#define POINT_H

#include<stdio.h>

double **allocate_points(int length);
void copy_point(double *src, double *dst);
void copy_points(double **src, int length, double **dst);
void free_points(double **points);

void print_null_point();
void print_zero_point();
void print_point(double *point);
void print_points(double **points, int length);
void print_point_i(double *point);
void print_points_i(double **points, int length);
void print_point_helper(FILE *fp, const char *format, double *point);
void print_points_helper(FILE *fp, const char *format,
			 double **points, int length);

void pp_arith(char type, double *point1, double *point2, double *res);
void ps_arith(char type, double *point1, double s, double *res);

void point_cartesian_to_spherical(double *cart, double *spher);
void points_cartesian_to_spherical(double **points, int length);
void point_spherical_to_cartesian(double *spher, double *cart);
void points_spherical_to_cartesian(double **points, int length);

void add_point(double **points, int length, double *point);
void box_points(double **points, int length, double *box);
void center_of_gravity_points(double **points, int length, double *rG);
int connected_particles(double *particle, double **points, int length,
			double radius);
int intersecting_particles(double *particle, double **points, int length,
			   double radius);
int is_free_point(double **points, int length, double *point);
double max_radius_point(double **points, int length, double *max_point);
int number_of_duplicates(double **points, int length, double *point);
int point_eq(double *point1, double *point2);
void scale_points(double **points, int length, double s);
void translate_points(double **points, int length, double *point);
void weigh_mean_point(double *p1, double w1,
		      double *p2, double w2, double *res);

/* Macros for arithmetic operations */
#define PP_PLUS(U, V, W) \
	(W)[0] = (U)[0] + (V)[0]; \
	(W)[1] = (U)[1] + (V)[1]; \
	(W)[2] = (U)[2] + (V)[2];

#define PP_MINUS(U, V, W) \
	(W)[0] = (U)[0] - (V)[0]; \
	(W)[1] = (U)[1] - (V)[1]; \
	(W)[2] = (U)[2] - (V)[2];

#define PP_TIMES(U, V, W) \
	(W)[0] = (U)[0] * (V)[0]; \
	(W)[1] = (U)[1] * (V)[1]; \
	(W)[2] = (U)[2] * (V)[2];

#define PP_DIVIDE(U, V, W) \
	(W)[0] = (U)[0] / (V)[0]; \
	(W)[1] = (U)[1] / (V)[1]; \
	(W)[2] = (U)[2] / (V)[2];

#define PS_PLUS(U, S, W) \
	(W)[0] = (U)[0] + (S); \
	(W)[1] = (U)[1] + (S); \
	(W)[2] = (U)[2] + (S);

#define PS_MINUS(U, S, W) \
	(W)[0] = (U)[0] - (S); \
	(W)[1] = (U)[1] - (S); \
	(W)[2] = (U)[2] - (S);

#define PS_TIMES(U, S, W) \
	(W)[0] = (U)[0] * (S); \
	(W)[1] = (U)[1] * (S); \
	(W)[2] = (U)[2] * (S);

#define PS_DIVIDE(U, S, W) \
	(W)[0] = (U)[0] / (S); \
	(W)[1] = (U)[1] / (S); \
	(W)[2] = (U)[2] / (S);

#endif

