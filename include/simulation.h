#ifndef SIMULATION_H
#define SIMULATION_H

struct Cluster simulate_cc(int particles, double df);
struct Cluster simulate_pc(int particles, double df, double kf);

#endif
