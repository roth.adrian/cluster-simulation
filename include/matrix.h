#ifndef MATRIX_H
#define MATRIX_H

struct Matrix {
	int rows;
	int cols;
	double **data;
};

struct Matrix allocate_matrix(int rows, int cols);
struct Matrix reallocate_matrix(struct Matrix m, int rows, int cols);
void copy_matrix(struct Matrix src, struct Matrix dst);
void free_matrix(struct Matrix m);
struct Matrix matrix_set(struct Matrix dst, int r, int c,
			 struct Matrix src);

void print_matrix(struct Matrix m);
struct Matrix read_matrix_from_file(const char *file);

struct Matrix eye(int n);
struct Matrix point_to_matrix(double *point);
void matrix_to_point(struct Matrix m, double *point);

struct Matrix dot(struct Matrix m1, struct Matrix m2);
struct Matrix dot2(struct Matrix m1, struct Matrix m2);
struct Matrix ms_add(struct Matrix m, double s);
struct Matrix ms_multiply(struct Matrix m, double s);

#endif
