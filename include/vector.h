#ifndef VECTOR_H
#define VECTOR_H

double mean(double *vec, int length);
void print_vector(double *vec, int length);
double *simple_linear_regression(double *x, double *y, int length,
			         double *beta);
double stdev(double *vec, int length);
double sum(double *vec, int length);
double sum_of_squares(double *vec, int length);

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#endif
