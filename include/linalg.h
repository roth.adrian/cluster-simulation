#ifndef LINALG_H
#define LINALG_H

struct Line {
	double point[3];
	double dir[3];
};

struct Line new_line(double* point, double *dir);

void cross_product(double *u, double *v, double *cp);
double scalar_product(double *u, double *v);
double norm_3d(double *u);

void orthogonal_dirs(double *e1, double *e2, double *e3);
struct Line reverse_line(struct Line l);
double shortest_distance(struct Line l, double *point);

/* Macros for some functions */
#define CROSS_PRODUCT(U, V, CP) \
	(CP)[0] = (U)[1] * (V)[2] - (U)[2] * (V)[1];\
	(CP)[1] = (U)[2] * (V)[0] - (U)[0] * (V)[2];\
	(CP)[2] = (U)[0] * (V)[1] - (U)[1] * (V)[0];

#define SCALAR_PRODUCT(U, V) \
	((U)[0] * (V)[0] + (U)[1] * (V)[1] + (U)[2] * (V)[2])

#define NORM(U) \
	(sqrt((U)[0] * (U)[0] + (U)[1] * (U)[1] + (U)[2] * (U)[2]))


#define CROSS_PRODUCT2D(U, V) \
	((U)[0] * (V)[1] - (U)[1] * (V)[0])

#define SCALAR_PRODUCT2D(U, V) \
	((U)[0] * (V)[0] + (U)[1] * (V)[1])

#define NORM2D(U) \
	(sqrt((U)[0] * (U)[0] + (U)[1] * (U)[1]))


#endif
