#ifndef COMPUTER_VISION_H
#define COMPUTER_VISION_H

struct Matrix;

struct Matrix camera_matrix(double *dir, double *point);
struct Matrix affine_camera_matrix(double *dir,
				   double *point);
struct Matrix intrinsic_matrix(double focal_length,
			       double scale_x, double scale_y,
			       double skew,
			       double principal_x, double principal_y);
struct Matrix project_points(double **points, int length,
			     struct Matrix camera);
struct Matrix transform_points2d(struct Matrix points, struct Matrix t);

struct Matrix rotation_x(double theta);
struct Matrix rotation_y(double theta);
struct Matrix rotation_z(double theta);
struct Matrix rotation_x_center(double theta, double x_c, double y_c);
struct Matrix rotation_y_center(double theta, double x_c, double y_c);
struct Matrix rotation_z_center(double theta, double x_c, double y_c);
struct Matrix translation(double x, double y);

#endif
