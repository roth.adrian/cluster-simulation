#ifndef GRID_H
#define GRID_H

struct Grid {
	double min[3];
	double size[3];
	double ***grid;
};

struct Grid allocate_grid(double *box);
void free_grid(struct Grid grid);

void print_grid(struct Grid grid);

void insert_grid(struct Grid grid, double *point);
double extract_grid(struct Grid grid, double *point);
void populate_grid(struct Grid grid, double **points, int length);

#endif
