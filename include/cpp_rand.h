#ifndef CPP_RAND_H
#define CPP_RAND_H

double cpp_randn();
double cpp_uniform_real();
int cpp_uniform_int(int min, int max);
double *shuffle(double *vec, int length);

#endif
