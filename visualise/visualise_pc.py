# -*- coding: utf-8 -*-
import argparse
import bpy
import numpy as np
import os
import sys

sys.path.append(os.path.dirname(__file__))
from cad_functions import Cluster

# check if background, then force rendering
background = '-b' in sys.argv

# all python arguments are after '--'
if '--' in sys.argv:
    sys.argv = sys.argv[sys.argv.index('--'):]
else:
    raise ValueError("Input file arguments after '--' flag to blender that will ignore everything afterwards")


default_output = 'visualise_pc.avi'
parser = argparse.ArgumentParser(description='Create a blender animation of the aggregate simuation procedure')
parser.add_argument('cluster_file', type=str,
                    help='the cluster-file to make a model of')
parser.add_argument('--output', '-o', type=str, default=default_output,
                    help='output file to render to, should be avi extension')
parser.add_argument('--scale', type=float, default=1,
                    help='scale size of the model, corresponds to the particle diameter')
parser.add_argument('--overlap-multiplier', type=float, default=1,
                    help='overlap of particles, good for 3D printing, default 1')
args = parser.parse_args()


everything = np.genfromtxt(args.cluster_file)
i = -1
n = 0
# Find the final cluster from the input
while n < 3:
    if np.isnan(everything[i, 0]):
        n += 1
    i -= 1

particles = everything[i + 2, 2] + 1
# Since we add single particles each step there will be n-1 sub
# clusters on the way since the first contains 2 particles
n_clusters = particles - 1

radius = args.scale / 2 * args.overlap_multiplier
clusters = []
# the current row in everything
curr = 0
# centre of gravity
cg = np.zeros(3)

# for each step of adding a particle (except the final one)
# there is a sub cluster with attempted insertions
for i in np.arange(n_clusters - 1):
    n = int(everything[curr, 2])
    # the plus ones are to avoid the header of each cluster
    points = everything[curr + 1: curr + 1 + n, :]
    curr += n + 1

    # B are the particle we're trying to attach on, this is not used
    B = everything[curr]
    curr += 2

    # A are the attempted locations for the new particle
    # if it is overlapping more attempts are performed
    A = np.zeros((0, 3))
    while not np.isnan(everything[curr, 0]):
        A = np.append(A, [everything[curr]], axis=0)
        # moving it relative to the centre of gravity which has been done in the c-code
        A[-1] += cg
        curr += 1
    curr += 1

    # Recalculating the center of gravity with the new added point
    cg = (cg * (i + 2) + A[-1]) / (i + 3)
    # special case for first iteration
    if i == 0:
        new_points = points
    clusters.append(Cluster(new_points, radius=radius, outer=A))
    # added new point which is corrected for the centre of gravity
    new_points = np.append(new_points, [A[-1]], axis=0)

# the final cluster with correct points
top = Cluster(new_points, radius=radius)

# create the clusters for each sub step and the particles to add
top.pc_init(clusters)

# Delete all default meshes
for scene in bpy.data.scenes:
        for obj in scene.objects:
            if obj.type == 'MESH':
                obj.select_set(True)
bpy.ops.object.delete()

# create animations
f = top.animate()
# set final frame from the returned number of frames for the animation
bpy.context.scene.frame_end = f + 24

if __name__ == '__main__' and (background or not args.output == default_output):
    print('rendering')
    # render to file and exit
    bpy.data.scenes['Scene'].render.filepath = args.output
    bpy.data.scenes['Scene'].render.image_settings.file_format = 'AVI_JPEG'
    bpy.ops.render.render(animation=True)
    exit()
