# -*- coding: utf-8 -*-
import argparse
import bpy
import numpy as np
import os
import sys

sys.path.append(os.path.dirname(__file__))
from cad_functions import Cluster

# check if background, then force rendering and exit
background = '-b' in sys.argv

# all python arguments are after '--'
if '--' in sys.argv:
    sys.argv = sys.argv[sys.argv.index('--'):]
else:
    raise ValueError("Input file arguments after '--' flag to blender that will ignore everything afterwards")


default_output = 'visualise_cc.avi'
parser = argparse.ArgumentParser(description='Create a blender animation of the aggregate simuation procedure')
parser.add_argument('cluster_file', type=str,
                    help='the cluster-file to make a model of')
parser.add_argument('--output', '-o', type=str, default=default_output,
                    help='output file to render to, should be avi extension')
parser.add_argument('--scale', type=float, default=1,
                    help='scale size of the model, corresponds to the particle diameter')
parser.add_argument('--overlap-multiplier', type=float, default=1,
                    help='overlap of particles, good for 3D printing, default 1')
args = parser.parse_args()


everything = np.genfromtxt(args.cluster_file, skip_footer=1)
i = -1
while everything[i, 0].is_integer() and everything[i, 1].is_integer():
    i -= 1

particles = everything[i, 2]
levels = np.log(particles) / np.log(2)
if not levels.is_integer():
    sys.exit('Could not find right number of particle in final cluster, try new simulation')

# Since only powers of two are allowed the number of sub aggregates steps are calculated like this
n_clusters = np.sum(particles / 2**np.arange(1, levels + 1))

radius = args.scale / 2 * args.overlap_multiplier
clusters = []
curr = 0
for i in np.arange(n_clusters - 1):
    # Cluster
    n = int(everything[curr, 2])
    points = everything[curr + 1: curr + 1 + n, :]
    curr += n + 1
    # Points on the outer to try
    n = int(everything[curr, 2])
    outer = everything[curr + 1: curr + 1 + n, :]
    curr += n + 1
    clusters.append(Cluster(points, radius=radius, outer=outer))

n = int(everything[curr, 2])
points = everything[curr + 1: curr + 1 + n, :]
top = Cluster(points, radius=radius)

top.cc_init(clusters)

# Delete all default meshes
for scene in bpy.data.scenes:
        for obj in scene.objects:
            if obj.type == 'MESH':
                obj.select_set(True)
bpy.ops.object.delete()


f = top.animate()
bpy.context.scene.frame_end = f + 24

if background or not args.output == default_output:
    print('rendering')
    # render to file and exit
    bpy.data.scenes['Scene'].render.filepath = args.output
    bpy.data.scenes['Scene'].render.image_settings.file_format = 'AVI_JPEG'
    bpy.ops.render.render(animation=True)
    exit()
