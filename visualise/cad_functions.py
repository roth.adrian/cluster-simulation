import bpy
from mathutils import Vector
import numpy as np


class Cluster:
    def calc_radius(points):
        if points.shape[0] > 1:
            t_points = Cluster.translate(points, -points[0])
            return np.min(np.linalg.norm(t_points[1:], axis=1)) / 2
        else:
            return 1 / 2

    # outer is connected to the simulation data for visualising the simulation procedure
    def __init__(self, points, radius=None, outer=None):
        self.points = points
        self.outer = outer
        self.sub1 = None
        self.sub2 = None
        self.obj = None
        if radius is None:
            self.radius = Cluster.calc_radius(points)
        else:
            self.radius = radius

    def __str__(self):
        ret = ""
        if self.sub1 is not None:
            ret += self.sub1.__str__()
        if self.sub2 is not None:
            ret += self.sub2.__str__()
        ret = '\n\t'.join(ret.splitlines())
        ret = '\n' + str(self.points) + ret
        return ret

    def pc_init(self, clusters):
        # creates the recursive structure for the pc simulation
        # this is later used for the animation
        length = len(clusters)
        if length != 0:
            self.sub1 = clusters[-1].pc_init(clusters[:-1])
            self.sub2 = Cluster(np.zeros((1, 3)), self.sub1.radius, np.zeros((1, 3)))
        return self

    def cc_init(self, clusters):
        length = len(clusters)
        if length != 0:
            mid = int((length - 2) / 2)
            self.sub1 = clusters[-2].cc_init(clusters[:mid])
            self.sub2 = clusters[-1].cc_init(clusters[mid:-2])
        return self

    def find_latest_added(base):
        objects = bpy.data.objects
        latest = None
        if base in objects:
            latest = objects[base]
        name_func = lambda i: '{0:s}.{1:03d}'.format(base, i)
        i = 1
        name = name_func(i)
        while name in objects:
            latest = objects[name]
            i += 1
            name = name_func(i)
        return latest

    def frames_per_config(length):
        return max(24 / length * 4, 15)

    def diff_point(length):
        return np.array([(length)**2 / 2, 0, 0])

    def merge(self, point):
        bpy.ops.object.empty_add(type='PLAIN_AXES', radius=1,
                                 location=point)
        
        self.obj = Cluster.find_latest_added('Empty')
        self.sub1.obj.parent = self.obj
        self.sub2.obj.parent = self.obj


    # recursive animation of clusters
    def rec_animate(self, point=np.zeros(3)):
        if self.sub1 is None or self.sub2 is None:
            return 0
        else:
            scene = bpy.context.scene
            # How many frames is this animation going to take
            fpc = Cluster.frames_per_config(self.points.shape[0])
            dp = Cluster.diff_point(self.points.shape[0])
            sub1_point = point
            sub2_point = point + dp

            f1 = self.sub1.rec_animate(sub1_point)
            f2 = self.sub2.rec_animate(sub2_point)
            f = max(f1, f2)
            if self.sub1.obj is None:
                self.sub1.obj = self.sub1.create(-dp)
            if self.sub2.obj is None:
                self.sub2.obj = self.sub2.create(dp)
            still = self.sub1.obj
            moving = self.sub2.obj
            self.merge(point)

            scene.frame_set(f)
            self.obj.keyframe_insert(data_path='location')
            self.sub1.obj.keyframe_insert(data_path='location')
            self.sub2.obj.keyframe_insert(data_path='location')
            f += fpc
            scene.frame_set(f)
            still.location -= still.location
            still.keyframe_insert(data_path='location')

            # do animation that tries all combinations of the outer points
            free_p = self.sub1.outer
            edge_p = self.sub2.outer
            for i in range(free_p.shape[0]):
                for j in range(edge_p.shape[0]):
                    scene.frame_set(f)
                    translation = still.location - moving.location + \
                        Vector(free_p[i]) - Vector(edge_p[j])
                    moving.location += translation
                    moving.keyframe_insert(data_path='location')
                    f += fpc

            scene.frame_set(f)
            # Set final location for the moving cluster
            moving.location = Vector(self.points[self.sub1.points.shape[0]])
            moving.keyframe_insert(data_path='location')
            f += 24
            return f

    def animated_deepest(self):
        if self.sub2 is None:
            return self.obj.location
        return self.obj.location + self.sub2.animated_deepest()

    def size(self):
        mini = np.min(self.points, axis=0)
        maxi = np.max(self.points, axis=0)
        return np.linalg.norm(maxi - mini)

    def animate_camera(self, f):
        if not 'Camera' in bpy.data.objects:
            bpy.ops.object.camera_add()
        camera = bpy.data.objects['Camera']
        camera.data.clip_end = 300
        bpy.ops.object.empty_add(type='PLAIN_AXES', radius=1,
                                 location=Vector([0, 0, 0]))
        mid = Cluster.find_latest_added('Empty')
        base_rotation = Vector([0, 70, 20]) * np.pi / 180
        mid.rotation_euler = base_rotation
        camera.parent = mid
        r = self.size() * 3
        camera.location = Vector([0, 0, r])

        ttc = camera.constraints.new(type='TRACK_TO')
        ttc.target = self.obj
        ttc.track_axis = 'TRACK_NEGATIVE_Z'
        ttc.up_axis = 'UP_Y'

        scene = bpy.context.scene
        scene.frame_set(0)
        mid.location = self.animated_deepest() + Vector([50, 0, 0])
        mid.keyframe_insert(data_path='location')
        scene.frame_set(int(f / 10))
        mid.location = Vector([0, 0, 0])
        mid.keyframe_insert(data_path='location')
        scene.frame_set(f / 12)
        mid.keyframe_insert(data_path='rotation_euler')
        scene.frame_set(f)
        mid.rotation_euler = base_rotation + Vector([0, 0, -f / 24 / 10 * np.pi / 2])
        mid.keyframe_insert(data_path='rotation_euler')

        # Fix lighting
        light_name = 'Light'
        if not light_name in bpy.data.objects:
            bpy.ops.object.light_add(type='POINT')
        bpy.data.objects[light_name].location = camera.location / 3
        bpy.data.objects[light_name].parent = mid

        return mid

    def animate(self):
        f = self.rec_animate()
        self.animate_camera(f)
        return f

    def translate(points, point):
        repeated = np.reshape(np.tile(point, points.shape[0]),
                              (points.shape[0], 3))
        return points + repeated

    def create(self, origin=np.zeros(3)):
        objects = []
        for i in range(self.points.shape[0]):
            bpy.ops.mesh.primitive_uv_sphere_add(radius=self.radius,
                                                 location=self.points[i])
            objects.append(Cluster.find_latest_added('Sphere'))

        if self.points.shape[0] > 1:
            bpy.ops.object.empty_add(type='PLAIN_AXES', radius=1,
                                     location=origin)
            parent_obj = Cluster.find_latest_added('Empty')
            for obj in objects:
                obj.parent = parent_obj
            c = {}
            c["object"] = c["active_object"] = objects[0]
            c["selected_objects"] = c["selected_editable_objects"] = objects
            bpy.ops.object.join(c)
        else:
            parent_obj = objects[0]
            parent_obj.location = origin
        return parent_obj
