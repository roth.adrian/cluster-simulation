#!/usr/bin/python3

# Inspired by stackoverflow 
# https://stackoverflow.com/questions/18344934/animate-a-rotating-3d-graph-in-matplotlib 

from matplotlib import pyplot as plt
from matplotlib import animation
from mpl_toolkits.mplot3d import Axes3D
import sys
from visualise_aggregate import plotAggregateFile


# Create a figure and a 3D Axes
file = sys.argv[1]
fig = plt.figure()
ax = Axes3D(fig)


# Create an init function and the animate functions.
# Both are explained in the tutorial. Since we are changing
# the the elevation and azimuth and no objects are really
# changed on the plot we don't have to return anything from
# the init and animate function. (return value is explained
# in the tutorial.
def init():
    plotAggregateFile(file, ax)


def animate(i):
    ax.view_init(elev=10., azim=2*i)

# Animate
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=180, interval=20, blit=True)
# Save
#   libav-tools package for ubuntu is needed for this part
anim.save(file + "_animation.mp4", fps=30, extra_args=['-vcodec', 'libx264'],
          writer="avconv", codec="libx264")
