# -*- coding: utf-8 -*-
import argparse
import bpy
import numpy as np
import os
import sys

sys.path.append(os.path.dirname(__file__))
from cad_functions import Cluster

# background argument to blender will force --print-to-obj and exit
if '-b' in sys.argv:
    sys.argv.append('--print-to-obj')

# all python arguments are after '--'
if '--' in sys.argv:
    sys.argv = sys.argv[sys.argv.index('--'):]
else:
    raise ValueError("Input file arguments after '--' flag to blender that will ignore everything afterwards")


parser = argparse.ArgumentParser(description='Create a 3D model for a simulated particle aggregate')
parser.add_argument('cluster_file', type=str,
                    help='the cluster-file to make a model of')
parser.add_argument('--scale', type=float, default=1,
                    help='scale size of the model, corresponds to the particle diameter')
parser.add_argument('--overlap-multiplier', type=float, default=1.25,
                    help='overlap of particles, good for 3D printing, default 1.25')
parser.add_argument('--print-to-obj', action='store_true',
                    help='print the resulting 3D model to a wavefront .obj file, this will also exit the blender session when finished')
args = parser.parse_args()

points = np.genfromtxt(args.cluster_file, skip_header=1)

radius = 0.5 * args.scale
points = points * 2 * radius
cluster = Cluster(points, radius=radius * (args.overlap_multiplier))

# Clearing scene
for scene in bpy.data.scenes:
        for obj in scene.objects:
            obj.select_set(True)
bpy.ops.object.delete()

cluster.create()

if args.print_to_obj:
    bpy.ops.export_scene.obj(filepath=args.cluster_file + '.obj')

    print('Sucess')
    sys.exit()
