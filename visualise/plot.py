#!/usr/bin/python3
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys
from visualise_aggregate import plotAggregateFile

file = sys.argv[1]
fig = plt.figure()
ax = Axes3D(fig)
plotAggregateFile(file, ax)
plt.savefig(file + '_plot.png')
# plt.show()
plt.close()
