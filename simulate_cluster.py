import os
import os.path as osp
import numpy as np
import subprocess

_file_dir = osp.dirname(osp.realpath(__file__))
# path to executable

# temporary file to write to
_temp_file = 'temp.txt';

def simulate_cluster(n, df, kf=None, algorithm='pc'):
    _c_path = osp.join(_file_dir, 'simulate_{}.out'.format(algorithm))
    simulate_cmd = '{} {} {} {}'.format(_c_path, n, df, _temp_file)
    if not kf is None and algorithm == 'pc':
        # only works with pc algorithm
        simulate_cmd = '{} -k {}'.format(simulate_cmd, kf)
    subprocess.run(simulate_cmd, stdout=subprocess.DEVNULL, shell=True, check=True)
    cluster = np.loadtxt(_temp_file)
    os.remove(_temp_file)
    return cluster
