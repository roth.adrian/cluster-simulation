#include<stdlib.h>
#include<stdio.h>
#include<string.h>

#include"grid.h"
#include"point.h"

struct Grid allocate_grid(double *box) {
	int i, j, xs, ys, zs;
	struct Grid o_grid;
	double ***grid;

	memcpy(o_grid.min, box, 3 * sizeof(double));
	pp_arith('-', box + 3, box, o_grid.size);
	xs = (int) o_grid.size[0];
	ys = (int) o_grid.size[1];
	zs = (int) o_grid.size[2];
	grid = (double ***)calloc(1, (xs + xs * ys) * sizeof(double *) +
				xs * ys * zs * sizeof(double));
	grid[0] = (double **)(grid + xs);
	grid[0][0] = (double *)(grid[0] + xs * ys);
	for (i = 0; i < xs; i++) {
		if (i != 0) {
			grid[i] = grid[i - 1] + ys;
			grid[i][0] = grid[i - 1][ys - 1] + zs;
		}
		for (j = 1; j < ys; j++) {
			grid[i][j] = grid[i][j - 1] + zs;
		}
	}
	o_grid.grid = grid;
	return o_grid;
}

void free_grid(struct Grid grid) {
	free(grid.grid);
}

void print_grid(struct Grid grid) {
	int i, j, k;
	double point[3];
	print_point_i(grid.min);
	print_point_i(grid.size);
	for (i = 0; i < grid.size[0]; i++) {
		for (j = 0; j < grid.size[1]; j++) {
			for (k = 0; k < grid.size[2]; k++) {
				point[0] = i;
				point[1] = j;
				point[2] = k;
				printf("%f ", extract_grid(grid,
							   point));
			}
			printf("\n");
		}
		printf("\n");
	}
}

static double *index_grid(struct Grid grid, double *point) {
	int xs, ys, zs, x, y, z;
	double new_p[3];

	pp_arith('-', point, grid.min, new_p);
	xs = (int) grid.size[0];
	ys = (int) grid.size[1];
	zs = (int) grid.size[2];
	x = (int) new_p[0];
	y = (int) new_p[1];
	z = (int) new_p[2];
	if (x < xs && x >= 0 &&
	    y < ys && y >= 0 &&
	    z < zs && z >= 0) {
		return &grid.grid[x][y][z];
	} else {
		return NULL;
	}
}

void insert_grid(struct Grid grid, double *point) {
	double *p;
	p = index_grid(grid, point);
	if (p != NULL) (*p)++;
}

double extract_grid(struct Grid grid, double *point) {
	double *p;
	p = index_grid(grid, point);
	if (p != NULL) {
		return *p;
	} else {
		return 0;
	}
}

void populate_grid(struct Grid grid, double **points, int length) {
	int i;
	for (i = 0; i < length; i++) {
		insert_grid(grid, points[i]);
	}
}
