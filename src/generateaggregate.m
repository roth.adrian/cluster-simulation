function [aggregate,RadGyr] = generateaggregate(p,frac)
%
% This function generates a soot particle with 2^p spheres
% We follow the algorithm described in  Thouy and Jullien, "A
% cluster-cluster aggregation model with tuneablew fractal dimension" J. of
% Phys. A: Math. Gen. (1994)
% The cluster consists of 2^p particles.
% fractal is the fractal dimension of the cluster
%
% We first create the 2^p-1 clusters with two spheres
% pos(m,n,x,y,z)=the position (x,y,z) of sphere n in cluster m
% x,y and z are integers
% neigh(m,n,x,y,z)=the position (x,y,z) of the neighbouring position number n in cluster m
%
tic;
% The factor k is used in deltagamma
k=2*sqrt(4^(1/frac)-1);
%
% Three different orientations of the dimers (aggregate with two spheres)
%
postype1(1,:)=[0 0 0];
postype1(2,:)=[1 0 0];
postype2(1,:)=[0 0 0];
postype2(2,:)=[0 1 0];
postype3(1,:)=[0 0 0];
postype3(2,:)=[0 0 1];
%
% The neighbouring sites of the three types of dimers
%
neightype1(1,:)=[-1,0,0];
neightype1(2,:)=[0,1,0];
neightype1(3,:)=[0,-1,0];
neightype1(4,:)=[0,0,1];
neightype1(5,:)=[0,0,-1];
neightype1(6,:)=[2,0,0];
neightype1(7,:)=[1,1,0];
neightype1(8,:)=[1,-1,0];
neightype1(9,:)=[1,0,1];
neightype1(10,:)=[1,0,-1];
neightype2(1,:)=[0,-1,0];
neightype2(2,:)=[1,0,0];
neightype2(3,:)=[-1,0,0];
neightype2(4,:)=[0,0,1];
neightype2(5,:)=[0,0,-1];
neightype2(6,:)=[0,2,0];
neightype2(7,:)=[1,1,0];
neightype2(8,:)=[-1,1,0];
neightype2(9,:)=[0,1,1];
neightype2(10,:)=[0,1,-1];
neightype3(1,:)=[0,0,-1];
neightype3(2,:)=[1,0,0];
neightype3(3,:)=[-1,0,0];
neightype3(4,:)=[0,1,0];
neightype3(5,:)=[0,-1,0];
neightype3(6,:)=[0,0,2];
neightype3(7,:)=[1,0,1];
neightype3(8,:)=[-1,0,1];
neightype3(9,:)=[0,1,1];
neightype3(10,:)=[0,-1,1];
%
% There are 2^(p-1) dimers and the types of these are random
%
for nant=1:2^(p-1)
    randa=rand(1);
    if randa<=1/3
        posn(nant,1,:)=postype1(1,:);
        posn(nant,2,:)=postype1(2,:);
        for mant=1:10
            neighn(nant,mant,:)=neightype1(mant,:);
        end
    elseif randa>=2/3
        posn(nant,1,:)=postype2(1,:);
        posn(nant,2,:)=postype2(2,:);
        for mant=1:10
            neighn(nant,mant,:)=neightype2(mant,:);
        end
    else
        posn(nant,1,:)=postype3(1,:);
        posn(nant,2,:)=postype3(2,:);
        for mant=1:10
            neighn(nant,mant,:)=neightype3(mant,:);
        end
    end
end
% R2Nsquare=Square of radius of gyration for a dimer
R2Nsquare(1:2^(p-1))=0.25;
% Now the cluster is formed
for np=1:p-1
    clear pos
    clear neigh
    pos=posn;
    neigh=neighn;
    clear posn
    clear neighn
    Ns=2^np;
% Ns=number of surface sites for each incoming cluster
    clear RNsquare
% RNsquare=square of radius of gyration of the present 2^(p-np) clusters
    RNsquare=R2Nsquare;
    clear R2Nsquare
% m is running through half of the clusters. They are then put together
% with the other half
    for m=1:2^(p-np-1)
        deltaGammaold=1e30;
% Na2=number of neighbour-surface sites for cluster 2
        Na2=length(neigh(2^(p-np)-m+1,:,1,1,1));
        for mpos1=1:Ns
% Each surface site of cluster one is placed at each neighbour-surface site
% of cluster 2. For each combination we first check if any surface sites overlap. If there are overlapping
% sites then we continue with the next combination.
            for mneigh2=1:Na2
% Now we check if any surface sites of cluster number m and number 2^(p-np)-m+1 overlap
                overlap=0;
                 Am=pos(m,:,:)-pos(m,mpos1*ones(Ns,1),:);
                 Bm=pos(2^(p-np)-m+1,:,:)-neigh(2^(p-np)-m+1,mneigh2*ones(Ns,1),:);
                 if Am-Bm==zeros(1,Ns,3)
                     overlap=1;
                 end
% If there is no overlap then the new cluster is formed
%posnew2=zeros(Ns,3);
                if overlap==0
                    posnew(1:Ns,:)=pos(m,:,:)-pos(m,mpos1*ones(Ns,1),:)+neigh(2^(p-np)-m+1,mneigh2*ones(Ns,1),:);
                    posnew(Ns+1:2*Ns,:)=pos(2^(p-np)-m+1,:,:);
%
% We  check if the value of deltaGamma is smaller than the former value. If
% it is, then this cluster becomes the new best cluster
%
                    RG1=sum(posnew(1:Ns,:))/Ns;
                    RG2=sum(posnew(Ns+1:2*Ns,:))/Ns;
                    Gamma=RG1-RG2;
                    Gammasquare=Gamma(1)^2+Gamma(2)^2+Gamma(3)^2;
                    deltaGamma=(Gammasquare-0.5*k^2*(RNsquare(m)+RNsquare(2^(p-np)-m+1))-1)^2+1e-6*rand(1);
                    if deltaGamma<=deltaGammaold
                        posn(m,1:2*Ns,:)=posnew(1:2*Ns,:);
                        deltaGammaold=deltaGamma;
                        R2Nsquare(m)=0.5*(RNsquare(m)+RNsquare(2^(p-np)-m+1))+0.25*Gammasquare;
                    end
                end
            end
        end
% Now we have the best cluster for this value of m. It has positions at posn
% We calculate the positions neighn for the neighbour surface sites for
% this cluster
% There are six positions around a surface position and all of these are checked if it is
% occupied or not
        mn=1;
        for npos1=1:2*Ns;
            npa=0;
            for npos2=1:2*Ns;
                kasta=posn(m,npos1,:);
                kasta(1)=kasta(1)-1;
                if kasta==posn(m,npos2,:)
                    npa=1;
                end
            end
            if npa==0
                neighn(m,mn,1:3)=posn(m,npos1,1:3);
                neighn(m,mn,1)=neighn(m,mn,1)-1;
                mn=mn+1;
            end
%
            npa=0;
            kasta=posn(m,npos1,:);
            kasta(1)=kasta(1)+1;
            for npos2=1:2*Ns;
                if kasta==posn(m,npos2,:)
                    npa=1;
                end
            end
            if npa==0
                neighn(m,mn,:)=posn(m,npos1,:);
                neighn(m,mn,1)=neighn(m,mn,1)+1;
                mn=mn+1;
            end
%
            npa=0;
            kasta=posn(m,npos1,:);
            kasta(2)=kasta(2)-1;
            for npos2=1:2*Ns;
                if kasta==posn(m,npos2,:)
                    npa=1;
                end
            end
            if npa==0
                neighn(m,mn,:)=posn(m,npos1,:);%+[0,-1,0];
                neighn(m,mn,2)=neighn(m,mn,2)-1;
                mn=mn+1;
            end
%
            npa=0;
            kasta=posn(m,npos1,:);
            kasta(2)=kasta(2)+1;
            for npos2=1:2*Ns;
                if kasta==posn(m,npos2,:)
                    npa=1;
                end
            end
            if npa==0
                neighn(m,mn,:)=posn(m,npos1,:);
                neighn(m,mn,2)=neighn(m,mn,2)+1;
                mn=mn+1;
            end
%
            npa=0;
            kasta=posn(m,npos1,:);
            kasta(3)=kasta(3)-1;
            for npos2=1:2*Ns;
                if kasta==posn(m,npos2,:)
                   npa=1;
                end
            end
            if npa==0
                neighn(m,mn,:)=posn(m,npos1,:);%+[0,0,-1];
                neighn(m,mn,3)=neighn(m,mn,3)-1;
                mn=mn+1;
            end
%
            npa=0;
            kasta=posn(m,npos1,:);
            kasta(3)=kasta(3)+1;
            for npos2=1:2*Ns;
                if kasta==posn(m,npos2,:)
                    npa=1;
                end
            end
            if npa==0
                neighn(m,mn,:)=posn(m,npos1,:);%+[0,0,1];
                neighn(m,mn,3)=neighn(m,mn,3)+1;
                mn=mn+1;
            end
        end
    end
end
aggregate(1:2*Ns,:)=posn(1,1:2*Ns,:);
%
% The aggregate is plotted
% nsphere=number of latitude and longitude lines in the spheres
%
nsphere=31;
figure(80)
    [X,Y,Z]=sphere(nsphere);
    X=0.5*X+aggregate(1,1);
    Y=0.5*Y+aggregate(1,2);
    Z=0.5*Z+aggregate(1,3);
    surf(X,Y,Z)
hold
for nnn=2:2^p
    [X,Y,Z]=sphere(nsphere);
    X=0.5*X+aggregate(nnn,1);
    Y=0.5*Y+aggregate(nnn,2);
    Z=0.5*Z+aggregate(nnn,3);
    surf(X,Y,Z)
end
        axis equal
RadGyr=2*sqrt(R2Nsquare);
Ns=2^p;
title(['\fontsize{12}',num2str(Ns),' spheres: fD=',num2str(frac),' RGyr/Rsph=',num2str(RadGyr)])
hold off
colormap autumn
toc;
