#include<math.h>
#include<stdio.h>
#include<stdlib.h>

#include"vector.h"

double mean(double *vec, int length) {
	double s;
	s = sum(vec, length);
	return s / length;
}

void print_vector(double *vec, int length) {
	int i;
	for (i = 0; i < length - 1; i++) {
		printf("%3.2f ", *vec++);
	}
	printf("%3.2f\n", *vec);
}

/*
double *shuffle(double *vec, int length) {
	int i, r;
	double temp;
	for (i = length - 1; i > 0; i--) {
		r = rand() % (i + 1);
		temp = vec[i];
		vec[i] = vec[r];
		vec[r] = temp;
	}
	return vec;
}
*/

double *simple_linear_regression(double *x, double *y, int length,
			      double *beta) {
	int i;
	double xmean, ymean, Sxx, Sxy;
	xmean = mean(x, length);
	ymean = mean(y, length);
	Sxx = 0;
	Sxy = 0;
	for (i = 0; i < length; i++) {
		Sxx += (x[i] - xmean) * (x[i] - xmean);
		Sxy += (x[i] - xmean) * (y[i] - ymean);
	}
	beta[1] = Sxy / Sxx;
	beta[0] = ymean - beta[1] * xmean;
	return beta;
}

double stdev(double *vec, int length) {
	int i;
	double acc, mean_val;
	mean_val = mean(vec, length);
	acc = 0;
	for (i = 0; i < length; i++) {
		acc += (*vec - mean_val) * (*vec - mean_val);
		vec++;
	}
	acc /= length - 1;
	return sqrt(acc);
}

double sum(double *vec, int length) {
	int i;
	double acc;
	acc = 0;
	for (i = 0; i < length; i++) {
		acc += *vec++;
	}
	return acc;
}

double sum_of_squares(double *vec, int length) {
	int i;
	double acc;
	acc = 0;
	for (i = 0; i < length; i++) {
		acc += *vec * *vec;
		vec++;
	}
	return acc;
}
