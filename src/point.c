#include<limits.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"point.h"
#include"linalg.h"
#include"vector.h"

static const double tol_eq = 1e-5;
static const double tol = 1e-2;

double **allocate_points(int length) {
	int i;
	double **points;
	points = (double **) calloc(1, length * sizeof(double *) +
				length * 3 * sizeof(double));

	points[0] = (double *)(points + length);
	for (i = 1; i < length; i++) {
	    points[i] = points[i - 1] + 3;
	}
	return points;
}

void copy_point(double *src, double *dst) {
	dst[0] = src[0];
	dst[1] = src[1];
	dst[2] = src[2];
}

void copy_points(double **src, int length, double **dst) {
	memcpy(dst[0], src[0], length * 3 * sizeof(double));
}


void free_points(double **points) {
	free(points);
}

void print_null_point() {
	printf("%.2f\t%.2f\t%.2f\n", NAN, NAN, NAN);
}

void print_zero_point() {
	printf("%d\t%d\t%d\n", 0, 0, 0);
}

void print_point(double *point) {
	print_point_helper(stdout, "%.2f\t%.2f\t%.2f\n", point);
}

void print_points(double **points, int length) {
	int i;
	for (i = 0; i < length; i++) {
		print_point(points[i]);
	}
}

void print_point_i(double *point) {
	print_point_helper(stdout, "%.0f\t%.0f\t%.0f\n", point);
}

void print_points_i(double **points, int length) {
	int i;
	for (i = 0; i < length; i++) {
		print_point_i(points[i]);
	}
}

void print_point_helper(FILE *fp, const char *format, double *point) {
	fprintf(fp, format, point[0], point[1], point[2]);
}

void print_points_helper(FILE *fp, const char *format,
			 double **points, int length) {
	int i;
	for (i = 0; i < length; i++) {
		print_point_helper(fp, format, points[i]);
	}
}

void pp_arith(char type, double *point1, double *point2, double *res) {
	switch (type) {
		case '+':
			res[0] = point1[0] + point2[0];
			res[1] = point1[1] + point2[1];
			res[2] = point1[2] + point2[2];
			break;
		case '-':
			res[0] = point1[0] - point2[0];
			res[1] = point1[1] - point2[1];
			res[2] = point1[2] - point2[2];
			break;
		case '*':
			res[0] = point1[0] * point2[0];
			res[1] = point1[1] * point2[1];
			res[2] = point1[2] * point2[2];
			break;
		case '/':
			res[0] = point1[0] / point2[0];
			res[1] = point1[1] / point2[1];
			res[2] = point1[2] / point2[2];
			break;
	}
}

void ps_arith(char type, double *point, double s, double *res) {
	switch (type) {
		case '+':
			res[0] = point[0] + s;
			res[1] = point[1] + s;
			res[2] = point[2] + s;
			break;
		case '-':
			res[0] = point[0] - s;
			res[1] = point[1] - s;
			res[2] = point[2] - s;
			break;
		case '*':
			res[0] = point[0] * s;
			res[1] = point[1] * s;
			res[2] = point[2] * s;
			break;
		case '/':
			res[0] = point[0] / s;
			res[1] = point[1] / s;
			res[2] = point[2] / s;
			break;
	}
}


void point_cartesian_to_spherical(double *cart, double *spher) {
	double r, phi, theta;
	r = NORM(cart);
	theta = acos(cart[2] / r);
	phi = atan2(cart[1], cart[0]);
	spher[0] = r;
	spher[1] = theta;
	spher[2] = phi;
}
void points_cartesian_to_spherical(double **points, int length) {
	int i;
	for (i = 0; i < length; i++) {
		point_cartesian_to_spherical(points[i], points[i]);
	}
}

void point_spherical_to_cartesian(double *spher, double *cart) {
	double x, y, z;
	x = spher[0] * sin(spher[1]) * cos(spher[2]);
	y = spher[0] * sin(spher[1]) * sin(spher[2]);
	z = spher[0] * cos(spher[1]);
	cart[0] = x;
	cart[1] = y;
	cart[2] = z;
}

void points_spherical_to_cartesian(double **points, int length) {
	int i;
	for (i = 0; i < length; i++) {
		point_spherical_to_cartesian(points[i], points[i]);
	}
}


void add_point(double **points, int length, double *point){
	points[length][0] = point[0];
	points[length][1] = point[1];
	points[length][2] = point[2];
}

void box_points(double **points, int length, double *box) {
	int i;
	for (i = 0; i < 3; i++) {
		box[i] = INT_MAX;
		box[3 + i] = INT_MIN;
	}
	for (i = 0; i < length; i++) {
		box[0] = MIN(points[i][0], box[0]);
		box[1] = MIN(points[i][1], box[1]);
		box[2] = MIN(points[i][2], box[2]);
		box[3] = MAX(points[i][0], box[3]);
		box[4] = MAX(points[i][1], box[4]);
		box[5] = MAX(points[i][2], box[5]);
	}
	ps_arith('+', &box[3], 1, &box[3]);
}


void center_of_gravity_points(double **points, int length, double *rG) {
	int i;
	double sum[3];
	sum[0] = 0;
	sum[1] = 0;
	sum[2] = 0;
	for (i = 0; i < length; i++) {
		pp_arith('+', sum, points[i], sum);
	}
	ps_arith('/', sum, length, rG);
}

int connected_particles(double *particle, double **points, int length,
			double radius) {
	int i, connections;
	double norm_diff, diff[3];
	connections = 0;
	for (i = 0; i < length; i++) {
		PP_MINUS(particle, points[i], diff);
		norm_diff = NORM(diff);
		if (norm_diff < 2 * radius  + tol &&
		    norm_diff > 2 * radius - tol) connections++;
	}
	return connections;
}

int intersecting_particles(double *particle, double **points, int length,
			   double radius) {
	int i, intersections;
	double diff[3];
	intersections = 0;
	for (i = 0; i < length; i++) {
		PP_MINUS(particle, points[i], diff);
		if (NORM(diff) < 2 * radius - tol) intersections++;
	}
	return intersections;
}

int is_free_point(double **points, int length, double *point) {
	int i;
	for (i = 0; i < length; i++) {
		if (point_eq(point, points[i])) {
			return 0;
		}
	}
	return 1;
}

double max_radius_point(double **points, int length, double *max_point) {
	int i;
	double r, mr;
	mr = 0;
	for (i = 0; i < length; i++) {
		r = NORM(points[i]);
		if (r > mr) {
			mr = r;
			copy_point(points[i], max_point);
		}
	}
	return mr;
}

int number_of_duplicates(double **points, int length, double *point) {
	int i, number;
	number = 0;
	for (i = 0; i < length; i++) {
		if (point_eq(point, points[i])) {
			number++;
		}
	}
	return number;
}

int point_eq(double *point1, double *point2) {
	int i;
	for (i = 0; i < 3; i++) {
		if (fabs(point1[i] - point2[i]) > tol_eq) {
			return 0;
		}
	}
	return 1;
}

void scale_points(double **points, int length, double s) {
	int i;
	for (i = 0; i < length; i++) {
		ps_arith('*', points[i], s, points[i]);
	}
}

void translate_points(double **points, int length, double *point) {
	int i;
	for (i = 0; i < length; i++) {
		pp_arith('+', point, points[i], points[i]);
	}
}

void weigh_mean_point(double *p1, double w1, double *p2, double w2,
		      double *res) {
	double sum[3], w_p1[3], w_p2[3];

	ps_arith('*', p1, w1, w_p1);
	ps_arith('*', p2, w2, w_p2);
	pp_arith('+', w_p1, w_p2, sum);
	ps_arith('/', sum, w1 + w2, res);
}
