#include<ctype.h>
#include<limits.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<math.h>

#include"cluster.h"
#include"point.h"

struct Cluster allocate_cluster(int length) {
	struct Cluster clust;
	clust.rG = (double *)calloc(3, sizeof(double));
	clust.points = allocate_points(length);
	clust.length = 0;
	clust.alloc_length = length;
	return clust;
}

struct Cluster copy_cluster(struct Cluster clust) {
	int length;
	struct Cluster copy;

	length = clust.length;
	copy = allocate_cluster(length);
	copy.df = clust.df;
	memcpy(copy.rG, clust.rG, 3 * sizeof(double));
	copy.RG2 = clust.RG2;
	copy.length = length;
	copy_points(clust.points, length, copy.points);
	return copy;
}

void free_cluster(struct Cluster clust) {
	free_points(clust.points);
	free(clust.rG);
}

static void print_cluster_helper(FILE *fp, struct Cluster clust) {
	fprintf(fp, "%.2f\t%.2f\t%d\n",
			clust.df, clust.RG2, clust.length);
	print_points_helper(fp, "%.2f\t%.2f\t%.2f\n",
			    clust.points, clust.length);
}

void print_cluster(struct Cluster clust) {
	print_cluster_helper(stdout, clust);
}

void print_cluster_to_file(const char *file, struct Cluster clust) {
	FILE *fp;
	fp = fopen(file, "w");
	print_cluster_helper(fp, clust);
	fclose(fp);
}

static int read_int(FILE *fp) {
	int c, num, sign;

	while((c = fgetc(fp)) == '\t');

	if (c == '-') {
		sign = -1;
		c = fgetc(fp);
	} else {
		sign = 1;
	}

	num = 0;
	while(isdigit(c)) {
		num = num * 10 + (int)(c - '0');
		c = fgetc(fp);
	}
	return num * sign;
}

static void read_error() {
	fprintf(stderr, "Error opening or reading file in cluster read\n");
	exit(1);
}

struct Cluster read_cluster_from_file(const char *file){
	int i, length;
	double point[3];
	double df, RG2;
	FILE *fp;
	struct Cluster clust;

	fp = fopen(file, "r");
	if (!fp) read_error();
	if (fscanf(fp, "%lf\t%lf\t", &df, &RG2) != 2) read_error();
	length = read_int(fp);

	clust = allocate_cluster(length);
	clust.df = df;
	clust.RG2 = RG2;
	for (i = 0; i < length; i++) {
		if (fscanf(fp, "%lf\t%lf\t%lf", point,
			   point + 1, point + 2) != 3) read_error();
		add_point_cluster(&clust, point);
	}
	center_of_gravity_points(clust.points, clust.length, clust.rG);

	fclose(fp);

	return clust;
}

void add_point_cluster(struct Cluster *clust, double *point) {
	add_point(clust->points, clust->length, point);
	clust->length++;
}

void translate_cluster(struct Cluster clust, double *point) {
	translate_points(clust.points, clust.length, point);
	pp_arith('+', point, clust.rG, clust.rG);
}
