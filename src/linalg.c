#include<math.h>
#include<stdio.h>
#include<string.h>

#include"point.h"
#include"linalg.h"

struct Line new_line(double* point, double *dir) {
	struct Line l;
	memcpy(l.point, point, 3 * sizeof(double));
	memcpy(l.dir, dir, 3 * sizeof(double));
	return l;
}


void cross_product(double *u, double *v, double *cp) {
	cp[0] = u[1] * v[2] - u[2] * v[1];
	cp[1] = u[2] * v[0] - u[0] * v[2];
	cp[2] = u[0] * v[1] - u[1] * v[0];
}

double scalar_product(double *u, double *v) {
	double inter[3];
	pp_arith('*', u, v, inter);
	return inter[0] + inter[1] + inter[2];
}

double norm_3d(double *u) {
	return sqrt(scalar_product(u, u));
}

void orthogonal_dirs(double *e1, double *e2, double *e3) {
	double temp[3];
	temp[0] = e1[1];
	temp[1] = e1[2];
	temp[2] = e1[0];
	cross_product(temp, e1, e2);
	ps_arith('/', e2, norm_3d(e2), e2);
	cross_product(e1, e2, e3);
}

struct Line reverse_line(struct Line l) {
	struct Line ret;
	ret = new_line(l.point, l.dir);
	ret.dir[0] = -ret.dir[0];
	ret.dir[1] = -ret.dir[1];
	ret.dir[2] = -ret.dir[2];
	return ret;
}

double shortest_distance(struct Line l, double *point) {
	double v[3], u[3], d[3];
	double s;
	pp_arith('-', point, l.point, v);
	s = scalar_product(v, l.dir);
	ps_arith('*', l.dir, s, u);
	pp_arith('-', v, u, d);
	return sqrt(d[0] * d[0] + d[1] * d[1] +
		    d[2] * d[2]);
}
