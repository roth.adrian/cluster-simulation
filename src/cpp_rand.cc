#include<algorithm>
#include<random>
#include<stdlib.h>
#include<vector>

# ifdef DEBUG
// remove randomness for debugging
static std::mt19937 mt(0);
# else
static std::random_device rd;
static std::mt19937 mt(rd());
#endif
static std::normal_distribution<double> norm_dist(0.0, 1.0);
static std::uniform_real_distribution<double> real_dist(0.0, 1.0);

extern "C" double cpp_randn()
{
	return norm_dist(mt);
}

extern "C" double cpp_uniform_real()
{
	return real_dist(mt);
}

extern "C" int cpp_uniform_int(int min, int max)
{
	std::uniform_int_distribution<int> int_dist(min, max);
	return int_dist(mt);
}

extern "C" double *shuffle(double *vec, int length)
{
	int i;
	std::vector<int> inds;
	double *copy;
	copy = (double *)malloc(sizeof(double) * length);
	for (int i = 0; i < length; i++) {
		copy[i] = vec[i];
		inds.push_back(i);
	}
	std::shuffle(inds.begin(), inds.end(), mt);
	for (i = 0; i < length; i++) {
		vec[i] = copy[inds[i]];
	}
	free(copy);
	return vec;
}
