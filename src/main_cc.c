#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include"cluster.h"
#include"simulation.h"

int main(int argc, char *argv[]) {
	int particles;
	double df;
	struct Cluster clust;
	clock_t start, end;

	switch (argc) {
		case 5:
		case 4:
		case 3:
			particles = strtol(argv[1], NULL, 10);
			df = strtod(argv[2], NULL);
			break;
		case 2:
			particles = strtod(argv[1], NULL);
			df = 2;
			break;
		default:
			particles = 32;
			df = 2;
			break;

	}

	start = clock();
	clust = simulate_cc(particles, df);
	end = clock();

	if (argc >= 4) {
		print_cluster_to_file(argv[3], clust);
#ifdef VISUALISE
		print_cluster(clust);
#endif

	} else {
		print_cluster(clust);
		printf("\n");

	}
	printf("# Excecution time: %8.3f seconds\n",
	       ((double) (end - start)) / CLOCKS_PER_SEC);
	free_cluster(clust);
	return 0;
}
