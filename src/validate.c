#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include"cluster.h"
#include"grid.h"
#include"point.h"

static const double rp = 0.5;

static int point_exists(double *point, double **points, int length) {
	return is_free_point(points, length, point) == 0;
}

static int point_singular(double *point, double **points, int length) {
	return number_of_duplicates(points, length, point) == 1;
}

static int point_connected(double *point, double **points, int length) {
	return connected_particles(point, points, length, rp) != 0;
}

static int point_intersecting(double *point, double **points, int length) {
	return intersecting_particles(point, points, length, rp) == 1;
}


static void valid_points(struct Cluster clust) {
	int i, invalid_e, invalid_s, invalid_c, invalid_i;

	invalid_e = 0;
	invalid_s = 0;
	invalid_c = 0;
	invalid_i = 0;
	for (i = 0; i < clust.length; i++) {
		if (!point_exists(clust.points[i], clust.points, clust.length)) {
			/* print_point_i(clust.points[i]);
			printf("Point does not exist in grid\n"); */
			invalid_e++;
		}
		if (!point_singular(clust.points[i], clust.points, clust.length)) {
			/* print_point(clust.points[i]);
			printf("Point is found more than one time\n"); */
			invalid_s++;
		}
		if (!point_connected(clust.points[i], clust.points, clust.length)) {
			/* print_point(clust.points[i]);
			printf("Point not valid in grid\n"); */
			invalid_c++;
		}
		if (!point_intersecting(clust.points[i], clust.points, clust.length)) {
			/* print_point(clust.points[i]);
			printf("Point not valid in grid\n"); */
			invalid_i++;
		}
	}
	if (invalid_e + invalid_s + invalid_c + invalid_i != 0) {
		printf("\nSummary for invalid points\n");
		printf("Non existing: %d\n", invalid_e);
		printf("Not singular: %d\n", invalid_s);
		printf("Not connected: %d\n", invalid_c);
		printf("Intersecting : %d\n", invalid_i);
	} else {
		printf("Points are valid\n");
	}
}

int main(int argc, char *argv[]) {
	struct Cluster clust;

	if (argc != 2) {
		fprintf(stderr, "Missing input cluster to verify");
		return 1;
	}
	clust = read_cluster_from_file(argv[1]);
	valid_points(clust);
	free_cluster(clust);
	return 0;
}

