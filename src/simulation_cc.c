#include<float.h>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

#include"cluster.h"
#include"cpp_rand.h"
#include"grid.h"
#include"linalg.h"
#include"point.h"
#include"simulation.h"

#define FREE 0
#define EDGE 1

static struct Cluster base_cluster(int particles, double df) {
	struct Cluster base;
	int i;

	if (particles == 2) {
		base = allocate_cluster(2);

		i = cpp_uniform_int(0, 2);
		base.points[1][i] = 1;
		base.rG[i] = 1. / 2;

		base.df = df;
		base.RG2 = 1. / 4;
		base.length = 2;
	} else {
		base = allocate_cluster(1);
		base.df = df;
		base.RG2 = 1;
		base.length = 1;
	}
	return base;
}

static int try_add_point(struct Cluster clust,
			 double *point,
			 struct Cluster *acc_clust,
			 int type,
			 int i,
			 int j) {
	int f;
	f = is_free_point(clust.points, clust.length, point);
	if (type == FREE && f) {
		add_point_cluster(acc_clust, point);
		return 0;
	} else if (type == EDGE && f) {
		point[i] -= j;
		add_point_cluster(acc_clust, point);
		return 1;
	} else {
		return 0;
	}
}

static void free_adjacent(struct Cluster clust,
			  double *point,
			  struct Cluster *acc_clust,
			  int type) {
	int i, j, done;
	done = 0;
	for (i = 0; i < 3; i++) {
		for (j = -1; j <= 1; j += 2) {
			point[i] += j;
			if (!done) {
				done = try_add_point(clust, point,
						acc_clust, type, i, j);
			}
			point[i] -= j;
		}
		if (done) break;
	}
}

static struct Cluster find_free_points(struct Cluster clust) {
	struct Cluster free_clust;
	int i, max_length;
	double point[3];
	size_t point_size;

	point_size = 3 * sizeof(double);
	max_length = clust.length * 6;
	free_clust = allocate_cluster(max_length);
	for (i = 0; i < clust.length; i++) {
		memcpy(point, clust.points[i], point_size);
		free_adjacent(clust, point, &free_clust, FREE);
	}
	return free_clust;
}

static struct Cluster find_edge_points(struct Cluster clust) {
	struct Cluster edge_clust;
	int i;
	double point[3];
	size_t point_size;

	point_size = 3 * sizeof(double);
	edge_clust = allocate_cluster(clust.length);
	for (i = 0; i < clust.length; i++) {
		memcpy(point, clust.points[i], point_size);
		free_adjacent(clust, point, &edge_clust, EDGE);
	}
	return edge_clust;
}

/*
static int collision(struct Cluster clust1, struct Cluster clust2) {
	int i;
	for (i = 0; i < clust1.length; i++) {
		if (!is_free_point(clust2.points, clust2.length,
					clust1.points[i])) {
			return 1;
		}
	}
	return 0;
}
*/

/* Optimised version of the collision function */
static int o_collision(struct Grid grid, struct Cluster clust2) {
	int i;
	for (i = 0; i < clust2.length; i++) {
		if (extract_grid(grid, clust2.points[i])) {
			return 1;
		}
	}
	return 0;
}

static double calc_Gamma2(double *rG1, double *rG2) {
	double Gamma[3];
	pp_arith('-', rG1, rG2, Gamma);
	return scalar_product(Gamma, Gamma);
}

static double calc_deltaGamma(struct Cluster clust1,
			      struct Cluster clust2) {
	double k2, Gamma2, RG12, RG22;
	RG12 = clust1.RG2;
	RG22 = clust2.RG2;
	Gamma2 = calc_Gamma2(clust1.rG, clust2.rG);
	k2 = 4 * (pow(4, 1. / clust1.df) - 1);
	return pow(Gamma2 - 1. / 2 * k2 * (RG12 + RG22) - 1, 2);
}

static struct Cluster merge(struct Cluster clust1,
			    struct Cluster clust2) {
	int i, length;
	double Gamma2;
	struct Cluster merged_agg;
	length = clust1.length + clust2.length;
	merged_agg = allocate_cluster(length);
	for (i = 0; i < clust1.length; i++) {
		add_point_cluster(&merged_agg, clust1.points[i]);
	}
	for (i = 0; i < clust2.length; i++) {
		add_point_cluster(&merged_agg, clust2.points[i]);
	}
	weigh_mean_point(clust1.rG, clust1.length, clust2.rG, clust2.length,
			 merged_agg.rG);
	Gamma2 = calc_Gamma2(clust1.rG, clust2.rG);
	merged_agg.RG2 = (clust1.RG2 + clust2.RG2) / 2. + Gamma2 / 4.;
	merged_agg.df = clust1.df;
	return merged_agg;
}

static void try_merge(struct Cluster clust1,
		      struct Cluster clust2,
	       	      double *point1,
	       	      double *point2,
	       	      double *min,
	       	      struct Cluster *collided,
		      struct Grid grid) {
	double trans_point[3];
	double deltaGamma;
	struct Cluster trans_agg;

	pp_arith('-', point1, point2, trans_point);
	trans_agg = copy_cluster(clust2);
	translate_cluster(trans_agg, trans_point);
	if (!o_collision(grid, trans_agg)) {
		deltaGamma = calc_deltaGamma(clust1, trans_agg);
		/* Keep the cluster with the most similar df */
		if (deltaGamma < *min) {
			free_cluster(*collided);
			*collided = merge(clust1, trans_agg);
			*min = deltaGamma;
		}
	}
	free_cluster(trans_agg);
}

static struct Cluster collide(struct Cluster clust1,
			      struct Cluster clust2) {
	int i, j;
	double min;
	struct Cluster collided, free_p, edge_p;
	struct Grid grid;
	double box[6];

	box_points(clust1.points, clust1.length, box);
	grid = allocate_grid(box);
	populate_grid(grid, clust1.points, clust1.length);

	collided = allocate_cluster(1);
	free_p = find_free_points(clust1);
	edge_p = find_edge_points(clust2);
	min = DBL_MAX;
	for (i = 0; i < free_p.length; i++) {
		for (j = 0; j < edge_p.length; j++) {
			try_merge(clust1, clust2, free_p.points[i],
			          edge_p.points[j], &min, &collided,
				  grid);
		}
	}
#ifdef VISUALISE
	print_cluster(clust1);
	print_cluster(free_p);
	print_cluster(clust2);
	print_cluster(edge_p);
#endif
	free_cluster(free_p);
	free_cluster(edge_p);
	free_cluster(clust1);
	free_cluster(clust2);
	free_grid(grid);
	return collided;
}

struct Cluster simulate_cc(int particles, double df) {
	if (particles == 1) {
		printf("Warning: Number of particles is not a power of 2 which might mess with the fractal dimension.\n\n");
		return base_cluster(1, df);
	} else if (particles == 2) {
		return base_cluster(2, df);
	} else {
		int part;
		struct Cluster clust1, clust2;
		part = particles / 2;
		clust1 = simulate_cc(part, df);
		clust2 = simulate_cc(particles - part, df);
		return collide(clust1, clust2);
	}
}

/*
struct Cluster simulate(int particles, double df) {
	int i, j, base_clusters, levels;
	struct Cluster *aggs;
	struct Cluster ret;

	levels = (int) log2((double) particles);
	particles = pow(2, (double) levels);
	base_clusters = particles / 2;
	aggs = malloc(base_clusters * sizeof(struct Cluster));

	for (i = 0; i < base_clusters; i++) {
		aggs[i] = base_cluster(2, df);
	}
	for (i = 2; i <= levels; i++) {
		for (j = 0; j < particles / exp2(i); j++) {
			aggs[j] = collide(aggs[2 * j],
					  aggs[2 * j + 1]);
		}
	}

	ret = aggs[0];
	free(aggs);
	return ret;
}
*/
