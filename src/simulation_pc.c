#include<math.h>
#include<stdlib.h>
#include<stdio.h>

#include"cluster.h"
#include"computer_vision.h"
#include"cpp_rand.h"
#include"grid.h"
#include"linalg.h"
#include"matrix.h"
#include"point.h"
#include"vector.h"
#include"simulation.h"

static const double rp = 0.5;
//static const double kf = 1.593;

void base_cluster(struct Cluster *base) {
	double sphere[3], cg[3];
	sphere[0] = 1;
	sphere[1] = cpp_uniform_real() * 2 * M_PI;
	sphere[2] = cpp_uniform_real() * M_PI;
	point_spherical_to_cartesian(sphere, base->points[1]);
	base->length = 2;
	PS_DIVIDE(base->points[1], -2, cg);
	translate_cluster(*base, cg);
	base->RG2 = NAN;
}

double calc_new_radius(double np, double df, double kf) {
	double r2;
	r2 = (np * np * rp * rp) / (np - 1) * pow(np / kf, 2 / df) -
	     (np * rp * rp) / (np - 1) -
	     (np * rp * rp) * pow((np - 1) / kf, 2 / df);
	return sqrt(r2);
}

int find_particles(double radius, struct Cluster clust, double *index_out) {
	int i, l;
	l = 0;
	for (i = 0; i < clust.length; i++) {
		if (fabs(NORM(clust.points[i]) - radius) < 2 * rp) {
			index_out[l++] = i;
		}
	}
	return l;
}

double calc_alpha(double *point, double radius) {
	double p_length;
	p_length = NORM(point);
	return acos((radius * radius + p_length * p_length -
		     4 * rp * rp) / (2 * radius * p_length));
}

void rand_new_particle(double *B, double radius,
		       double alpha, double *A) {
	double phi, sphere[3];
	struct Matrix Ry, Rz, R, m_A, m_A2;
	phi = cpp_uniform_real() * 2 * M_PI;
	point_cartesian_to_spherical(B, sphere);
	A[0] = radius;
	A[1] = alpha;
	A[2] = phi;
	point_spherical_to_cartesian(A, A);
	Ry = rotation_y(sphere[1]);
	Rz = rotation_z(sphere[2]);
	m_A = point_to_matrix(A);
	R = dot(Rz, Ry);
	m_A2 = dot(R, m_A);
	matrix_to_point(m_A2, A);
	free_matrix(Ry);
	free_matrix(Rz);
	free_matrix(R);
	free_matrix(m_A);
	free_matrix(m_A2);
}

int find_particle_location(double radius, double *ind, int ind_length,
			    double *A, struct Cluster clust) {
	int i, n, intersections;
	double *B, alpha;
	intersections = 1;
	for (i = 0; i < ind_length && intersections != 0; i++) {
		B = clust.points[(int)ind[i]];
		alpha = calc_alpha(B, radius);
#ifdef VISUALISE
		print_point(B);
		print_null_point();
#endif
		for (n = 0; n < 25 && intersections != 0; n++) {
			rand_new_particle(B, radius, alpha, A);
			intersections = intersecting_particles(A,
						clust.points,
						clust.length,
						rp);
#ifdef VISUALISE
			print_point(A);
#endif
		}
	}
	return intersections;
}

double add_particle(struct Cluster *clust, int n_particles, double kf) {
	int ind_length, intersections;
	double *ind; /* Particles on the radius */
	double radius;
	double A[3], cg[3], max_point[3];

	radius = calc_new_radius(n_particles + 1, clust->df, kf);
	max_radius_point(clust->points, clust->length, max_point);
	if (radius > NORM(max_point) + 2 * rp) {
		// Trying to set particle with too large radius
		point_cartesian_to_spherical(max_point, max_point);
		max_point[0] += 2 * rp;
		point_spherical_to_cartesian(max_point, A);
	} else {
		ind = (double *)malloc(clust->length * sizeof(double));
		do {
			ind_length = find_particles(radius, *clust, ind);
			shuffle(ind, ind_length);
			intersections = find_particle_location(radius,
						ind, ind_length, A,
						*clust);
			radius += 0.1;
		} while (intersections != 0); // if true radius too low
		free(ind);
	}
	add_point_cluster(clust, A);
	PS_DIVIDE(A, -clust->length, cg);
	translate_cluster(*clust, cg);
	return radius;
}

struct Cluster simulate_pc(int particles, double df, double kf) {
	int j;
	struct Cluster clust;

	clust = allocate_cluster(particles);
	base_cluster(&clust);
	clust.df = df;
	for (j = 2; j < particles; j++) {
#ifdef VISUALISE
		print_cluster(clust);
#endif
		add_particle(&clust, j, kf);
#ifdef VISUALISE
		print_null_point();
#endif
	}
	return clust;
}
