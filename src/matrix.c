#include<assert.h>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

#include"matrix.h"

struct Matrix allocate_matrix(int rows, int cols) {
	struct Matrix m;
	int i;
	m.data = (double **)calloc(1, rows * sizeof(double *) +
				rows * cols * sizeof(double));

	m.data[0] = (double *)(m.data + rows);
	for (i = 1; i < rows; i++) {
	    m.data[i] = m.data[i - 1] + cols;
	}
	m.rows = rows;
	m.cols = cols;
	return m;
}

struct Matrix reallocate_matrix(struct Matrix m, int rows, int cols) {
	int i;
	struct Matrix new_m;
	new_m = allocate_matrix(rows, cols);
	for (i = 0; i < rows; i++) {
		memcpy(new_m.data[i], m.data[i], cols * sizeof(double));
	}
	free_matrix(m);
	return new_m;
}

void copy_matrix(struct Matrix src, struct Matrix dst) {
	memcpy(dst.data[0], src.data[0],
	       src.rows * src.cols * sizeof(double));
}

void free_matrix(struct Matrix m) {
	free(m.data);
}

struct Matrix matrix_set(struct Matrix dst, int r, int c,
			 struct Matrix src) {
	int i;
	assert(dst.rows >= r + src.rows &&
	       dst.cols >= c + src.cols);
	for (i = 0; i < src.rows; i++) {
		memcpy(dst.data[r + i] + c, src.data[i],
		       src.cols * sizeof(double));
	}
	return dst;
}

void print_matrix(struct Matrix m) {
	int i, j;
	for (i = 0; i < m.rows; i++) {
		for (j = 0; j < m.cols; j++) {
			printf("%4.2f ", m.data[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

static void read_error() {
	fprintf(stderr, "Error opening or reading file in matrix read\n");
	exit(1);
}


struct Matrix read_matrix_from_file(const char *file) {
	int i, j, rows, cols;
	FILE *fp;
	struct Matrix m;
	double *row;

	fp = fopen(file, "r");
	if (!fp) read_error();
	if (fscanf(fp, "%d %d", &rows, &cols) != 2) read_error();

	m = allocate_matrix(rows, cols);
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			row = m.data[i];
			if (fscanf(fp, "%lf ", row + j) != 1)
				read_error();
		}
	}
	fclose(fp);
	return m;
}

struct Matrix eye(int n) {
	struct Matrix m;
	int i;
	m = allocate_matrix(n, n);
	for (i = 0; i < n; i++) {
		m.data[i][i] = 1;
	}
	return m;
}

struct Matrix point_to_matrix(double *point) {
	struct Matrix m;
	m = allocate_matrix(3, 1);
	m.data[0][0] = point[0];
	m.data[1][0] = point[1];
	m.data[2][0] = point[2];
	return m;
}

void matrix_to_point(struct Matrix m, double *point) {
	point[0] = m.data[0][0];
	point[1] = m.data[0][1];
	point[2] = m.data[0][2];
}

struct Matrix dot(struct Matrix m1, struct Matrix m2) {
	struct Matrix m;
	int i, j, k;
	assert(m1.cols == m2.rows);
	m = allocate_matrix(m1.rows, m2.cols);
	for (i = 0; i < m1.rows; i++) {
		for (j = 0; j < m2.cols; j++) {
			for (k = 0; k < m1.cols; k++) {
				m.data[i][j] += m1.data[i][k] *
						m2.data[k][j];
			}
		}
	}
	return m;
}
struct Matrix dot2(struct Matrix m1, struct Matrix m2) {
	struct Matrix m;
	m = dot(m1, m2);
	free_matrix(m1);
	free_matrix(m2);
	return m;
}

struct Matrix ms_add(struct Matrix m, double s) {
	int i, j;
	for (i = 0; i < m.rows; i++) {
		for (j = 0; j < m.cols; j++) {
			m.data[i][j] += s;
		}
	}
	return m;
}
struct Matrix ms_multiply(struct Matrix m, double s) {
	int i, j;
	for (i = 0; i < m.rows; i++) {
		for (j = 0; j < m.cols; j++) {
			m.data[i][j] *= s;
		}
	}
	return m;
}
