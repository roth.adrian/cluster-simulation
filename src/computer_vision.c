#include<assert.h>

#include"computer_vision.h"
#include"math.h"
#include"matrix.h"
#include"point.h"

struct Matrix camera_matrix(double *dir, double *point) {
	struct Matrix camera, R_y, R_z, R, t, T;
	double sphere[3];
	camera = allocate_matrix(3, 4);
	point_cartesian_to_spherical(dir, sphere);
	R_y = rotation_y(-sphere[1]);
	R_z = rotation_z(-sphere[2]);
	R = dot2(R_y, R_z);
	matrix_set(camera, 0, 0, R);
	T = point_to_matrix(point);
	t = dot2(R, T);
	ms_multiply(t, -1);
	matrix_set(camera, 0, 3, t);
	free_matrix(t);
	return camera;
}

static struct Matrix affine_camera(struct Matrix camera) {
	camera.data[2][0] = 0;
	camera.data[2][1] = 0;
	camera.data[2][2] = 0;
	camera.data[2][3] = 1;
	return camera;
}

struct Matrix affine_camera_matrix( double *dir, double *point) {
	struct Matrix camera;
	camera = camera_matrix(dir, point);
	affine_camera(camera);
	return camera;
}

struct Matrix intrinsic_matrix(double f,
			       double scale_x, double scale_y,
			       double skew,
			       double principal_x, double principal_y) {
	struct Matrix K;
	K = allocate_matrix(3, 3);
	K.data[0][0] = f * scale_x;
	K.data[1][1] = f * scale_y;
	K.data[0][1] = skew;
	K.data[0][2] = principal_x;
	K.data[1][2] = principal_y;
	K.data[2][2] = 1;
	return K;
}

struct Matrix project_points(double **points, int length,
				struct Matrix camera) {
	int i;
	struct Matrix points2d;
	double norm;
	points2d = allocate_matrix(length, 2);
	for (i = 0; i < length; i++) {
		norm = (camera.data[2][0] * points[i][0] +
			camera.data[2][1] * points[i][1] +
			camera.data[2][2] * points[i][2]) +
			camera.data[2][3];
		points2d.data[i][0] =
			(camera.data[0][0] * points[i][0] +
			 camera.data[0][1] * points[i][1] +
			 camera.data[0][2] * points[i][2] +
			 camera.data[0][3]) / norm;
		points2d.data[i][1] =
			(camera.data[1][0] * points[i][0] +
			 camera.data[1][1] * points[i][1] +
			 camera.data[1][2] * points[i][2] +
			 camera.data[1][3]) / norm;
	}
	return points2d;
}

struct Matrix transform_points2d(struct Matrix points, struct Matrix t) {
	int i;
	struct Matrix t_points;
	double norm;
	assert(t.cols == t.rows);
	t_points = allocate_matrix(points.rows, points.cols);
	for (i = 0; i < points.rows; i++) {
		t_points.data[i][0] =
			t.data[0][0] * points.data[i][0] +
			t.data[0][1] * points.data[i][1];
		t_points.data[i][1] =
			t.data[1][0] * points.data[i][0] +
			t.data[1][1] * points.data[i][1];
		if (t.cols == 3) {
			norm = t.data[2][0] * points.data[i][0] +
			       t.data[2][1] * points.data[i][1] +
			       t.data[2][2];
			t_points.data[i][0] += t.data[0][2];
			t_points.data[i][1] += t.data[1][2];
			t_points.data[i][0] /= norm;
			t_points.data[i][1] /= norm;
		}
	}
	return t_points;
}


struct Matrix rotation_x(double theta) {
	struct Matrix m;
	m = allocate_matrix(3, 3);
	m.data[0][0] = 1;
	m.data[1][1] = cos(theta);
	m.data[1][2] = -sin(theta);
	m.data[2][1] = sin(theta);
	m.data[2][2] = cos(theta);
	return m;
}

struct Matrix rotation_y(double theta) {
	struct Matrix m;
	m = allocate_matrix(3, 3);
	m.data[0][0] = cos(theta);
	m.data[0][2] = sin(theta);
	m.data[1][1] = 1;
	m.data[2][0] = -sin(theta);
	m.data[2][2] = cos(theta);
	return m;
}

struct Matrix rotation_z(double theta) {
	struct Matrix m;
	m = allocate_matrix(3, 3);
	m.data[0][0] = cos(theta);
	m.data[0][1] = -sin(theta);
	m.data[1][0] = sin(theta);
	m.data[1][1] = cos(theta);
	m.data[2][2] = 1;
	return m;
}

struct Matrix rotation_x_center(double theta, double x_c, double y_c) {
	struct Matrix t1, m, t2;
	t1 = translation(-x_c, -y_c);
	m = rotation_x(theta);
	t2 = translation(x_c, y_c);
	return dot2(dot2(t2, m), t1);
}
struct Matrix rotation_y_center(double theta, double x_c, double y_c) {
	struct Matrix t1, m, t2;
	t1 = translation(-x_c, -y_c);
	m = rotation_y(theta);
	t2 = translation(x_c, y_c);
	return dot2(dot2(t2, m), t1);
}
struct Matrix rotation_z_center(double theta, double x_c, double y_c) {
	struct Matrix t1, m, t2;
	t1 = translation(-x_c, -y_c);
	m = rotation_z(theta);
	t2 = translation(x_c, y_c);
	return dot2(dot2(t2, m), t1);
}

struct Matrix translation(double x, double y) {
	struct Matrix m;
	m = eye(3);
	m.data[0][2] = x;
	m.data[1][2] = y;
	return m;
}
