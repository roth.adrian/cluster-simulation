#include<ctype.h>
#include<math.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#include"cluster.h"
#include"simulation.h"

/*
 * Before there was a possibility to have different fractal dimension for some
 * particles in the same aggregate, this was removed since it probably does not
 * follow the theory of the simulation method
static char *skip_padding(char *string) {
	while ((string[0] == ' ' || string[0] == ',') && string[0] != 0) {
		string++;
	}
	return string;
}

static void parse_particles(char *string, int **list, int *n) {
	string = skip_padding(string);
	if (string[0] == '[') {
		int maxn;
		char *endptr;
		*n = 0;
		maxn = 2;
		*list = malloc(maxn * sizeof(int));
		string = skip_padding(string + 1);
		do {
			if (*n == maxn) {
				maxn *= 2;
				*list = realloc(*list, maxn * sizeof(int));
			}
			(*list)[*n] = strtol(string, &endptr, 10);
			string = endptr;
			(*n)++;
			string = skip_padding(string);
		} while (isdigit(string[0]));
	} else {
		*list[0] = strtol(string, NULL, 10);
	}
}

static void parse_dfs(char *string, double **list, int n) {
	string = skip_padding(string);
	if (string[0] == '[' && n > 1) {
		int i;
		char *endptr;
		*list = malloc(n * sizeof(double));
		string = skip_padding(string + 1);
		for (i = 0; i < n; i++) {
			if (!isdigit(string[0])) format_error("At least as many df as particles", n);
			(*list)[i] = strtod(string, &endptr);
			string = endptr;
			string = skip_padding(string);
		}
	} else if (isdigit(string[0]) && n == 1) {
		*list[0] = strtod(string, NULL);
	} else {
		format_error("Wrong format of the df input argument in relation to the particles", n);
	}
}
*/

static void format_error(char *text, int n) {
	fprintf(stderr, "Error: %s %d\n", text, n);
	exit(1);
}


static const double default_kf = 1.593;
int main(int argc, char *argv[]) {
	int particles;
	double df, kf;
	struct Cluster clust;
	clock_t start, end;

	kf = -1;
	for (int i = 4; i < argc - 1; i++) {
		if (strcmp("-kf", argv[i]) == 0) {
			kf = strtod(argv[i + 1], NULL);
			break;
		}
	}
	if (kf < 0) {
		kf = default_kf;
	}

	switch (argc) {
		case 7:
		case 6:
		case 5:
		case 4:
		case 3:
			particles = strtol(argv[1], NULL, 10);
			df = strtod(argv[2], NULL);
			break;
		case 2:
			particles = strtod(argv[1], NULL);
			df = 2;
			break;
		default:
			particles = 32;
			df = 2;
			break;

	}

	if (df <= 1 || 3 <= df) {
		format_error("Fractal dimension are only valid between 1 and", 3);
	}

	start = clock();
	clust = simulate_pc(particles, df, kf);
	end = clock();

	if (argc >= 4) {
		print_cluster_to_file(argv[3], clust);

	} else {
		print_cluster(clust);
		printf("\n");

	}
	printf("# Excecution time: %8.3f seconds\n",
	       ((double) (end - start)) / CLOCKS_PER_SEC);
	free_cluster(clust);
	return 0;
}
