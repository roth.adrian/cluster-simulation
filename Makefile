
VPATH = src
CC = gcc
CFLAGS = $(EXTRA) -lm -O3 # -pg
CPPFLAGS = -MMD -Wextra -Wall -pedantic-errors -funsigned-char -Werror -Iinclude  -g

MAIN_SIM = simulate_pc.out

.PHONY: build run plot animate validate_cc validate_pc clean

build: simulate_cc.out simulate_pc.out validate.out output
-include *.d

OBJ_FILES = cluster.o computer_vision.o grid.o linalg.o matrix.o point.o vector.o cpp_rand.o
	

simulate_cc.out: main_cc.o simulation_cc.o $(OBJ_FILES)
	g++ -o $@ $^ $(CFLAGS)

simulate_pc.out: main_pc.o simulation_pc.o $(OBJ_FILES)
	g++ -o $@ $^ $(CFLAGS)


run: $(MAIN_SIM)
	./$<


# Examples and validation rules
RUNOUTPUT = output/cluster
N = 128
Df = 1.8
plot: $(MAIN_SIM) output
	./$< $(N) $(Df) $(RUNOUTPUT)
	python3 ../visualise/plot.py $(RUNOUTPUT)
	xdg-open $(RUNOUTPUT)_plot.pdf

animate: $(MAIN_SIM) output
	./$< $(N) $(Df) $(RUNOUTPUT)
	python3 ../visualise/animate.py $(RUNOUTPUT)
	vlc $(RUNOUTPUT)_animation.mp4 &

validate_pc: simulate_pc.out validate.out output
	./$< 200 2 $(RUNOUTPUT)
	./validate.out $(RUNOUTPUT)
	./$< "[150,50]" "[2,1.5]" $(RUNOUTPUT)
	./validate.out $(RUNOUTPUT)

validate_cc: simulate_cc.out validate.out output
	./$< 256 $(Df) $(RUNOUTPUT)
	./validate.out $(RUNOUTPUT)

validate.out: validate.o $(OBJ_FILES)
	g++ -o $@ $^ $(CFLAGS)

output:
	mkdir -p $@


clean:
	rm -f *.o *.d *.out
