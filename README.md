# Cluster-Cluster and Particle-Cluster aggregation written in C
This c-code implements two different aggregation simulation algorithms, one cluster-cluster (CC) and one particle-cluster (PC) algorithm.
The difference is that the CC merges clusters of particles in each step where the PC adds a single particle to a cluster in each step.
Both algorithms produce aggregates with equal sized particles with diameter 1.
The PC algorithm is preferred here since it is fast and quite accurate, the implementation is based on:

K. Skorupski, J. Mroczka, T. Wriedt, and N. Riefler. *A fast and accurate implementation of tunable algorithms used for generation of fractal-like aggregate models.* Physica A: Statistical Mechanics and its Applications, 404:106 – 117, 2014. doi:https://doi.org/10.1016/j.physa.2014.02.072.

Even though CC algorithms generally better describes real cluster formation it is harder to simulate.
To make it reasonable here, the assumption of only integer coordinates for particles.
The CC algorithm is based on:

R. Thouy and R. Jullien. *A cluster-cluster aggregation model with tunable fractal dimension.* Journal of Physics A: Mathematical and General, 27(9):2953–2963, may 1994. doi: https://doi.org/10.1088/0305-4470/27/9/012.

The implementation is a part of the [master-thesis project](https://www.lunduniversity.lu.se/lup/publication/8936472) *Image analysis to estimate the fractal dimension of soot aggregates* by Adrian Roth at Division of Combustion Physics, Faculty of Engineering, Lund University.
Pages 8-11 of the thesis describes the cluster simulation methods in more detail.

## Build
To build the c-code, gcc (compiler for c), g++ (compiler for c++) together with the program makefile (to execute the correct build commands) should be installed.
With the correct installation you can run `make` in the repository directory to build the full simulation program.
Suggestions for installations are found below.

### Windows
To include all requirements MinGW is recommended.
Follow the installation instructions here [MinGW-getting-started](http://mingw.org/wiki/Getting_Started).
In the installation manager install both `mingw32-base-bin` and `mingw32-gcc-g++-bin`.
Do not forget to add the programs installed to your path to be able to access them from the command prompt, [possible help](https://stackoverflow.com/questions/5733220/how-do-i-add-the-mingw-bin-directory-to-my-system-path).
Here you should use the command `mingw32-make.exe` instead of `make`

### Mac (not tested)
Install [Xcode](https://www.cyberciti.biz/faq/howto-apple-mac-os-x-install-gcc-compiler/) for gcc and g++ compilers.
To install makefile one can use [homebrew](https://brew.sh/) which is a package managing system for mac.
Write
```
brew install makefile
```
in a terminal.

### Linux
For Debian based distributions write
```
apt-get install gcc g++ makefile
```
in a terminal.
Otherwise use the appropriate package manager.


## Usage
There are different possibilities to run the simulations, first from command line you can write:
```
./simulate_pc.out N D_f [cluster_file] [-kf KF_VALUE]
```
where `N` is the number of particles for the aggregate and  `D_f` is the fractal dimension in the range `[1,3]`. If the `cluster_file` is not specified the simulated cluster will be printed to `stdout`. An option is to give a fractal prefactor `kf` value that differs from the default value of 1.593.

The output consists of `N+1` rows and three columns where the first row is a header with three values: the fractal dimension, the Radius of Gyration and the number of particles for the simulated aggregate.
`cluster_file` is a valid `csv` (tab delimited) file to be parsed in the programming language or program of your choice.
For `matlab` there is a wrapper function in `simulate_cluster.m` that takes a `N` and `D_f` as parameters and runs the binary executable to return the simulated cluster as a matrix with `N+1` rows and three columns.
For `python` there is a similar wrapper.
Both wrappers use the particle-cluster algorithm by default and an optional third parameter can be set as `'cc'` to use the cluster-cluster algorithm instead.

## Plot aggregate
To run the python plotting code, python with packages `matplotlib` and `numpy` are required.
```
python3 visualise/plot.py cluster_file
```
The plot is saved as `[cluster_file]_plot.png`.

## Blender, view in 3D
There is python code compatible with blender scripting (tested on version 2.8) to produce a 3D model of the aggregate,
```
blender --python visualise/cadit.py -- cluster_file
```
The `--` makes blender ignore everything after that flag.
Try `blender --python visualise/cadit.py -- -h` to see options for the script.

### Aggregation animation
Finally an animation for how the simulation algorithms work can be produced.
This is slightly more complicated since information from the simulation runtime needs to be printed to a file.
First rebuilt everything with:
```
make -B EXTRA=-DVISUALISE
```
Run the simulation and pipe `stdout` to a file such as:
```
./simulate_pc.out 32 2 cluster_file > full_cluster_simulation
```
Then you can run the following with blender to produce a session with the animation prepared and the animation rendered
```
blender --python visualise/visualise_pc.py -- full_cluster_simulation
```
Afterwards you should rebuild again to not get a lot of printed stuff in each simulation,
```
make -B
```
