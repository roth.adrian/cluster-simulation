function cluster = simulate_cluster(n, df, kf, algorithm)

if nargin < 4
  algorithm = 'pc';
end

% Converting input params to string
n_str = num2str(n);
df_str = num2str(df);

% get path of the file
m_path = mfilename('fullpath');
[path, ~, ~] = fileparts(m_path);
% Check if windows or linux path separator
if length(path) == 0 && filesep == '/'
  path = './';
else
  path = [path, filesep];
end
% path to the executable
c_path = [path, 'simulate_', algorithm, '.out'];

% Writing to temporary file
temp_file = 'temp.txt';
simulate_cmd = [c_path, ' ', n_str, ' ', df_str, ' ', temp_file];
if nargin > 2 && strcmp(algorithm, 'pc')
  % kf only compatible with pc algorithm
  simulate_cmd = [simulate_cmd, ' -kf ', num2str(kf)];
end

[retval, cmdout] = system(simulate_cmd);
if retval
  error(cmdout);
end

cluster = readmatrix(temp_file);
delete(temp_file);

end
